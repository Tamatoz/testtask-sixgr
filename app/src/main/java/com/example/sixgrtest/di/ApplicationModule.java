package com.example.sixgrtest.di;

import android.content.Context;

import com.example.sixgrtest.App;
import com.example.sixgrtest.Consts;
import com.example.sixgrtest.model.Repository;
import com.example.sixgrtest.model.RepositoryImpl;
import com.example.sixgrtest.model.api.Api;
import com.example.sixgrtest.utils.LiveDataCallAdapterFactory;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.vanniktech.rxpermission.RealRxPermission;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = {ViewModelModule.class})
public class ApplicationModule {


    @Provides
    static Api provideApi(){
        return new Retrofit.Builder()
                .baseUrl(Consts.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build().create(Api.class);
    }

    @Provides
    static Repository provideRepository(Api api){
        return new RepositoryImpl(api);
    }

    @Provides
    static RealRxPermission provideRxPermissions(Context context){
        return RealRxPermission.getInstance(context);
    }

    @Provides
    static FusedLocationProviderClient provideLocationProvider(Context context){
        return LocationServices.getFusedLocationProviderClient(context);
    }
}
