package com.example.sixgrtest.di;


import com.example.sixgrtest.viewmodel.DailyViewModel;
import com.example.sixgrtest.viewmodel.HourlyViewModel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HourlyViewModel.class)
    abstract ViewModel bindRandomViewModel(HourlyViewModel hourlyViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DailyViewModel.class)
    abstract ViewModel bindSettingsViewModel(DailyViewModel dailyViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

}