package com.example.sixgrtest.di;

import android.content.Context;

import com.example.sixgrtest.view.daily.DailyWeatherFragment;
import com.example.sixgrtest.view.hourly.HourlyWeatherFragment;
import com.example.sixgrtest.view.mainscreen.MainActivity;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance Builder applicationContext(Context context);
        ApplicationComponent build();
    }

    void inject(MainActivity activity);
    void inject(HourlyWeatherFragment fragment);
    void inject(DailyWeatherFragment fragment);
}
