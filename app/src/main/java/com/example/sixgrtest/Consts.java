package com.example.sixgrtest;

public class Consts {
    public static final String weatherKey = "e5d819e1176c9f2d1faa9d05e258a10e";
    public static final String baseUrl = String.format("https://api.darksky.net/forecast/%s/", weatherKey);
}
