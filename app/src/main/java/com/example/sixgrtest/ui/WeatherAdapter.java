package com.example.sixgrtest.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sixgrtest.R;
import com.example.sixgrtest.databinding.RowWeatherBinding;
import com.example.sixgrtest.model.pojo.SubWeatherModel;
import com.example.sixgrtest.model.pojo.WeatherDataModel;
import com.example.sixgrtest.model.pojo.WeatherModel;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {

    private List<WeatherDataModel> weather;
    private LayoutInflater layoutInflater;
    private boolean isDaily = false;
    private Context context;

    public WeatherAdapter(Context context, List<WeatherDataModel> weather, boolean isDaily) {
        this.weather = weather;
        this.context = context;
        this.isDaily = isDaily;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        RowWeatherBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.row_weather, parent, false);
        return new WeatherViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {
        holder.binding.setData(weather.get(position));
        holder.binding.setIsDaily(isDaily);
        holder.binding.getRoot().setBackgroundColor(
                (position % 2 ==0) ? Color.WHITE : ContextCompat.getColor(context, R.color.lightBlue));
    }

    @Override
    public int getItemCount() {
        return weather.size();
    }

    public static class WeatherViewHolder extends RecyclerView.ViewHolder{
        private final RowWeatherBinding binding;

        public WeatherViewHolder(final RowWeatherBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }
}
