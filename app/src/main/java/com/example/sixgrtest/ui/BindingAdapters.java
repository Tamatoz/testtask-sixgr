package com.example.sixgrtest.ui;

import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;
import androidx.databinding.BindingAdapter;

import com.example.sixgrtest.R;
import com.example.sixgrtest.utils.Utils;

import java.text.DecimalFormat;

public class BindingAdapters {

    @BindingAdapter({"app:windBearing", "app:windSpeed"})
    public static void setWindText(TextView textView, int bearing, float speed) {
        textView.setText(
                String.format(
                        "%s %d%s",
                        (speed == 0) ? textView.getContext().getString(R.string.weather_wind_direction_text_0)
                        : Utils.getWindStringResourceByBearing(bearing, textView.getContext()),
                        (int)speed,
                        textView.getContext().getString(R.string.windMetric)
                        )
        );
    }

    @BindingAdapter({"app:icon"})
    public static void setWeatherIcon(ImageView imageView, String icon) {
        if(icon == null) return;
        Integer iconRes = null;
        switch(icon){
            case "clear-day": iconRes = R.drawable.weather_1_clear_day; break;
            case "clear-night": iconRes = R.drawable.weather_2_clear_night; break;
            case "rain": iconRes = R.drawable.weather_3_rain; break;
            case "snow": iconRes = R.drawable.weather_4_snow; break;
            case "sleet": iconRes = R.drawable.weather_5_sleet; break;
            case "wind": iconRes = R.drawable.weather_6_wind; break;
            case "fog": iconRes = R.drawable.weather_7_fog; break;
            case "cloudy": iconRes = R.drawable.ic_w_cloudy; break;
            case "partly-cloudy-day": iconRes = R.drawable.weather_9_party_cloudy_day; break;
            case "partly-cloudy-night": iconRes = R.drawable.weather_10_party_cloudy_night; break;
            case "hail": iconRes = R.drawable.weather_11_hail; break;
            case "thunderstorm": iconRes = R.drawable.weather_12_thunderstorm; break;
            case "tornado": iconRes = R.drawable.weather_13_tornado; break;
        }
        if (iconRes != null) imageView.setImageResource(iconRes);
    }

    @BindingAdapter({"app:precipitation"})
    public static void setPrecText(TextView textView, float prec) {
        textView.setText(new DecimalFormat("#.##").format(prec) + " mm");
    }

    @BindingAdapter({"app:minTemp", "app:maxTemp", "app:temp", "app:isDaily"})
    public static void setTempText(TextView textView, float minTemp, float maxTemp, float temp, boolean isDaily) {
        String str = isDaily ? String.format(textView.getContext().getString(R.string.minMaxTempFormat),
                (int)minTemp,
                (int)maxTemp) : String.format(textView.getContext().getString(R.string.celsiusDegreeTemp),
                (int)temp);
        textView.setText(HtmlCompat.fromHtml(
                str,
                HtmlCompat.FROM_HTML_MODE_LEGACY
        ));
    }
}
