package com.example.sixgrtest.view.mainscreen;

import android.content.Context;
import android.location.Location;

import com.example.sixgrtest.R;
import com.example.sixgrtest.view.daily.DailyWeatherFragment;
import com.example.sixgrtest.view.hourly.HourlyWeatherFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private Location location;

    public PagerAdapter(@NonNull FragmentManager fm, Context context, Location location) {
        super(fm);
        this.context = context;
        this.location = location;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return HourlyWeatherFragment.newInstance(location);
            case 1: return DailyWeatherFragment.newInstance(location);
            default: return HourlyWeatherFragment.newInstance(location);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return context.getString(R.string.weather_hourly);
            case 1: return context.getString(R.string.weather_daily);
            default: return context.getString(R.string.weather_hourly);
        }
    }
}
