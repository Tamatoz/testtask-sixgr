package com.example.sixgrtest.view.mainscreen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.functions.Consumer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import com.example.sixgrtest.App;
import com.example.sixgrtest.R;
import com.example.sixgrtest.model.api.Api;
import com.example.sixgrtest.model.api.ApiResponse;
import com.example.sixgrtest.model.pojo.WeatherModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.vanniktech.rxpermission.Permission;
import com.vanniktech.rxpermission.RealRxPermission;

import javax.inject.Inject;

public class MainActivity extends FragmentActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabs;

    @Inject
    RealRxPermission realRxPermission;

    @Inject
    FusedLocationProviderClient fusedLocationProviderClient;

    private Disposable rxPermissionsDisposable = Disposables.empty();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplicationContext()).getApplicationComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        tabs.setupWithViewPager(viewPager);

        rxPermissionsDisposable = realRxPermission.requestEach(Manifest.permission.ACCESS_FINE_LOCATION).subscribe(permission -> {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(location ->
                        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), this, location)));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        rxPermissionsDisposable.dispose();
    }
}
