package com.example.sixgrtest.view.hourly;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sixgrtest.App;
import com.example.sixgrtest.BaseFragment;
import com.example.sixgrtest.R;
import com.example.sixgrtest.databinding.FragmentWeatherBinding;
import com.example.sixgrtest.model.api.ApiResponse;
import com.example.sixgrtest.model.pojo.WeatherModel;
import com.example.sixgrtest.ui.WeatherAdapter;
import com.example.sixgrtest.viewmodel.HourlyViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.sixgrtest.viewmodel.WeatherViewModel.ArgLat;
import static com.example.sixgrtest.viewmodel.WeatherViewModel.ArgLon;

public class HourlyWeatherFragment extends BaseFragment {
    private HourlyViewModel viewModel;

    @Inject
    HourlyViewModel.Factory viewModelFactory;

    public static HourlyWeatherFragment newInstance(Location location) {
        HourlyWeatherFragment fg = new HourlyWeatherFragment();
        Bundle args = new Bundle();
        args.putDouble(ArgLat, location.getLatitude());
        args.putDouble(ArgLon, location.getLongitude());
        fg.setArguments(args);
        return fg;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this, viewModelFactory.create(this)).get(HourlyViewModel.class);
//        viewModel.loadWeather().observe(this, weatherModelApiResponse -> {});
        FragmentWeatherBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        binding.setIsDaily(false);
        View view = binding.getRoot();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        viewModel.weather.observe(this, weatherModel ->
                binding.recyclerView.setAdapter(new WeatherAdapter(requireContext(), weatherModel.getHourly().getData(), false)));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.infoMessage.observe(this, this::showError);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ((App) context.getApplicationContext()).getApplicationComponent().inject(this);
    }

}
