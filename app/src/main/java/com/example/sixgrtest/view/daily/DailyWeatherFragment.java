package com.example.sixgrtest.view.daily;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sixgrtest.App;
import com.example.sixgrtest.BaseFragment;
import com.example.sixgrtest.R;
import com.example.sixgrtest.databinding.FragmentWeatherBinding;
import com.example.sixgrtest.model.api.ApiResponse;
import com.example.sixgrtest.model.pojo.WeatherModel;
import com.example.sixgrtest.ui.WeatherAdapter;
import com.example.sixgrtest.viewmodel.DailyViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import static com.example.sixgrtest.viewmodel.WeatherViewModel.ArgLat;
import static com.example.sixgrtest.viewmodel.WeatherViewModel.ArgLon;

public class DailyWeatherFragment extends BaseFragment {
    private DailyViewModel viewModel;

    @Inject
    DailyViewModel.Factory viewModelFactory;

    public static DailyWeatherFragment newInstance(Location location) {
        DailyWeatherFragment fg = new DailyWeatherFragment();
        Bundle args = new Bundle();
        args.putDouble(ArgLat, location.getLatitude());
        args.putDouble(ArgLon, location.getLongitude());
        fg.setArguments(args);
        return fg;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this, viewModelFactory.create(this)).get(DailyViewModel.class);
        FragmentWeatherBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weather, container, false);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        binding.setIsDaily(true);
        View view = binding.getRoot();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        viewModel.weather.observe(this, weatherModel -> {
            binding.recyclerView.setAdapter(new WeatherAdapter(requireContext(), weatherModel.getDaily().getData(), true));
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.infoMessage.observe(this, this::showError);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        ((App) context.getApplicationContext()).getApplicationComponent().inject(this);
    }

}
