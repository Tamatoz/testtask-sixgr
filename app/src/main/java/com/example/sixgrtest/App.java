package com.example.sixgrtest;

import android.app.Application;

import com.example.sixgrtest.di.ApplicationComponent;
import com.example.sixgrtest.di.DaggerApplicationComponent;

public class App extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationContext(getApplicationContext())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
