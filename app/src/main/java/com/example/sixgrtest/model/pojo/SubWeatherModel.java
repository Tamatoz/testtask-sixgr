package com.example.sixgrtest.model.pojo;

import java.util.ArrayList;

public class SubWeatherModel {
    private String summary;
    private String icon;
    private ArrayList<WeatherDataModel> data = new ArrayList <> ();


    // Getter Methods

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    // Setter Methods

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public ArrayList<WeatherDataModel> getData() {
        return data;
    }
}