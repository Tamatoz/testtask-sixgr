package com.example.sixgrtest.model;

import com.example.sixgrtest.model.api.ApiResponse;
import com.example.sixgrtest.model.pojo.WeatherModel;

import androidx.lifecycle.LiveData;

public interface Repository {

    LiveData<ApiResponse<WeatherModel>> getHourlyWeather(double lat, double lon);

    LiveData<ApiResponse<WeatherModel>> getDailyWeather(double lat, double lon);
}
