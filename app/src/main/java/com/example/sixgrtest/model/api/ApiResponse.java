package com.example.sixgrtest.model.api;

import java.io.IOException;
import retrofit2.Response;

public class ApiResponse<T> {

        public static <T> ApiErrorResponse<T> create(Throwable error)  {
            return new ApiErrorResponse<>(error.getMessage() == null ? "unknown error" : error.getMessage());
        }

        public static <T> ApiResponse<T>  create(Response<T> response){
            if (response.isSuccessful()) {
                T body = response.body();
                if (body == null) {
                    return new ApiEmptyResponse<>();
                } else {
                    return new ApiSuccessResponse<>(body);
                }
            } else {
                String  msg = null;
                try {
                    msg = response.errorBody().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String errorMsg = (msg == null || msg.isEmpty()) ? response.message() : msg;
                return new ApiErrorResponse<>((errorMsg == null) ? "unknown error" : errorMsg);
            }
        }

    public static class ApiErrorResponse<T> extends ApiResponse<T>{
        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public ApiErrorResponse(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        private String errorMessage;
    }

    public static class ApiSuccessResponse<T> extends ApiResponse<T>{
        public T getBody() {
            return body;
        }

        public void setBody(T body) {
            this.body = body;
        }

        public ApiSuccessResponse(T body) {
            this.body = body;
        }

        private T body;
    }

    public static class ApiEmptyResponse<T> extends ApiResponse<T>{}
}