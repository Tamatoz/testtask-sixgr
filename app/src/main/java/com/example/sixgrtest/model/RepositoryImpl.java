package com.example.sixgrtest.model;

import com.example.sixgrtest.model.api.Api;
import com.example.sixgrtest.model.api.ApiResponse;
import com.example.sixgrtest.model.pojo.WeatherModel;

import java.util.Arrays;
import java.util.Locale;

import androidx.lifecycle.LiveData;

public class RepositoryImpl implements Repository {

    private Api api;

    public RepositoryImpl(Api api) {
        this.api = api;
    }

    @Override
    public LiveData<ApiResponse<WeatherModel>> getHourlyWeather(double lat, double lon) {
        return api.getHourlyWeather(String.format(Locale.US,"%f,%f", lat, lon));
    }

    @Override
    public LiveData<ApiResponse<WeatherModel>> getDailyWeather(double lat, double lon) {
        return api.getDailyWeather(String.format(Locale.US, "%f,%f", lat, lon));
    }
}
