package com.example.sixgrtest.model.pojo;

import android.text.format.DateFormat;

import java.util.Calendar;

public class WeatherDataModel {
    private long time;
    private String summary;
    private String icon;
    private float precipIntensity;
    private float precipProbability;
    private float temperature;
    private float apparentTemperature;
    private float dewPoint;
    private float humidity;
    private float pressure;
    private float windSpeed;
    private float windGust;
    private int windBearing;
    private float cloudCover;
    private float uvIndex;
    private float visibility;
    private float ozone;


    public float getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(float temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    private float temperatureMin;
    private float temperatureMax;

    private String stringDate;
    private String stringHour;


    public String getStringDate() {
        if(stringDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(time*1000);
            String str = DateFormat.format("d MMM", cal).toString();
            setStringDate(str);
        }
        return stringDate;
    }

    public void setStringDate(String stringDate) {
        this.stringDate = stringDate;
    }



    public String getStringHour() {
        if(stringHour == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(time*1000);
            String str = DateFormat.format("h a", cal).toString();
            setStringHour(str);
        }
        return stringHour;
    }

    public void setStringHour(String stringHour) {
        this.stringHour = stringHour;
    }


    public float getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(float temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    // Getter Methods

    public Long getTime() {
        return time;
    }

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    public float getPrecipIntensity() {
        return precipIntensity;
    }

    public float getPrecipProbability() {
        return precipProbability;
    }

    public float getTemperature() {
        return temperature;
    }

    public float getApparentTemperature() {
        return apparentTemperature;
    }

    public float getDewPoint() {
        return dewPoint;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public float getWindGust() {
        return windGust;
    }

    public int getWindBearing() {
        return windBearing;
    }

    public float getCloudCover() {
        return cloudCover;
    }

    public float getUvIndex() {
        return uvIndex;
    }

    public float getVisibility() {
        return visibility;
    }

    public float getOzone() {
        return ozone;
    }

    // Setter Methods

    public void setTime(Long time) {
        this.time = time;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setPrecipIntensity(float precipIntensity) {
        this.precipIntensity = precipIntensity;
    }

    public void setPrecipProbability(float precipProbability) {
        this.precipProbability = precipProbability;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public void setApparentTemperature(float apparentTemperature) {
        this.apparentTemperature = apparentTemperature;
    }

    public void setDewPoint(float dewPoint) {
        this.dewPoint = dewPoint;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    public void setWindGust(float windGust) {
        this.windGust = windGust;
    }

    public void setWindBearing(int windBearing) {
        this.windBearing = windBearing;
    }

    public void setCloudCover(float cloudCover) {
        this.cloudCover = cloudCover;
    }

    public void setUvIndex(float uvIndex) {
        this.uvIndex = uvIndex;
    }

    public void setVisibility(float visibility) {
        this.visibility = visibility;
    }

    public void setOzone(float ozone) {
        this.ozone = ozone;
    }
}