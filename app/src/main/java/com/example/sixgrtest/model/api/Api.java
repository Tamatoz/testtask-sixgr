package com.example.sixgrtest.model.api;

import com.example.sixgrtest.model.pojo.WeatherModel;

import androidx.lifecycle.LiveData;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Api {

    @GET("{coordinates}?exclude=minutely,daily,alerts,flags&units=si")
    LiveData<ApiResponse<WeatherModel>> getHourlyWeather(@Path("coordinates") String latLng);

    @GET("{coordinates}?exclude=minutely,hourly,alerts,flags&units=si")
    LiveData<ApiResponse<WeatherModel>> getDailyWeather(@Path("coordinates") String latLng);
}
