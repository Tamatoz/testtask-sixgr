package com.example.sixgrtest.model.pojo;

import java.util.ArrayList;

public class WeatherModel {
    private float latitude;
    private float longitude;
    private String timezone;
    WeatherDataModel currently = new WeatherDataModel();
    SubWeatherModel hourly = new SubWeatherModel();
    SubWeatherModel daily = new SubWeatherModel();
    private float offset;


    // Getter Methods

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public WeatherDataModel getCurrently() {
        return currently;
    }

    public SubWeatherModel getHourly() {
        return hourly;
    }

    public SubWeatherModel getDaily() {
        return daily;
    }

    public float getOffset() {
        return offset;
    }

    // Setter Methods

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setCurrently(WeatherDataModel currentlyObject) {
        this.currently = currentlyObject;
    }

    public void setHourly(SubWeatherModel hourlyObject) {
        this.hourly = hourlyObject;
    }

    public void setDaily(SubWeatherModel dailyObject) {
        this.daily = dailyObject;
    }

    public void setOffset(float offset) {
        this.offset = offset;
    }
}