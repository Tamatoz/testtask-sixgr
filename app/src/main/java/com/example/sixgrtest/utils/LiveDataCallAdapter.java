package com.example.sixgrtest.utils;

import com.example.sixgrtest.model.api.ApiResponse;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;
import java.lang.reflect.Type;
import java.util.concurrent.atomic.AtomicBoolean;

public class LiveDataCallAdapter<R> implements CallAdapter<R, LiveData<ApiResponse<R>>> {
    public LiveDataCallAdapter(Type responseType) {
        this.responseType = responseType;
    }

    private Type responseType;

    @Override
    public Type responseType() {
        return responseType;
    }

    @Override
    public LiveData<ApiResponse<R>> adapt(Call<R> call) {
        return new LiveData<ApiResponse<R>>() {
            private AtomicBoolean started = new AtomicBoolean(false);
            @Override
            protected void onActive() {
                super.onActive();
                if (started.compareAndSet(false, true)) {
                    call.enqueue(new Callback<R>() {
                        @Override
                        public void onResponse(Call<R> call, Response<R> response) {
                            postValue(ApiResponse.create(response));
                        }

                        @Override
                        public void onFailure(Call<R> call, Throwable t) {
                            postValue(ApiResponse.create(t));
                        }
                    });
                }
            }

            @Override
            protected void onInactive() {
                super.onInactive();
                if (call.isExecuted()) call.cancel();
            }
        };
    }
}
