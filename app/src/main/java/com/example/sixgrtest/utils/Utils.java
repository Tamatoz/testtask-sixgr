package com.example.sixgrtest.utils;

import android.content.Context;

public class Utils {

    public static String getWindStringResourceByBearing(int bearing, Context context){
        String resName = String.format("weather_wind_direction_text_%d", Math.max(1, (int)(bearing / 11.25f)));
        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(resName, "string", packageName);
        return context.getString(resId);
    }
}
