package com.example.sixgrtest;

import android.location.Location;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment {

    protected void showError(String error){
        if(getView() != null) Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT).show();
    }

}
