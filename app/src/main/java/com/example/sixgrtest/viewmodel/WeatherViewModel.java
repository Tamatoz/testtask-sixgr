package com.example.sixgrtest.viewmodel;

import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AbstractSavedStateVMFactory;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.example.sixgrtest.model.Repository;
import com.example.sixgrtest.model.api.ApiResponse;
import com.example.sixgrtest.model.pojo.WeatherModel;
import com.example.sixgrtest.utils.SingleLiveEvent;

import javax.inject.Inject;

public abstract class WeatherViewModel extends ViewModel {

    public static final String ArgLat = "lat";
    public static final String ArgLon = "lon";

    protected Repository repository;
    protected SavedStateHandle savedStateHandle;

    public LiveData<WeatherModel> weather;
    public SingleLiveEvent<String> infoMessage = new SingleLiveEvent<>();

    public WeatherViewModel(Repository repository, SavedStateHandle savedStateHandle) {
        this.repository = repository;
        this.savedStateHandle = savedStateHandle;

        location = new Pair(savedStateHandle.get(ArgLat), savedStateHandle.get(ArgLon));

    }
    

    public Pair<Double, Double> location;

}
