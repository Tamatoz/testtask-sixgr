package com.example.sixgrtest.viewmodel;

import com.example.sixgrtest.BaseFragment;
import com.example.sixgrtest.model.Repository;
import com.example.sixgrtest.model.api.ApiResponse;
import com.example.sixgrtest.model.pojo.WeatherModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AbstractSavedStateVMFactory;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

public class HourlyViewModel extends WeatherViewModel {

    @Inject
    public HourlyViewModel(Repository repository, SavedStateHandle stateHandle) {
        super(repository, stateHandle);
        weather = Transformations.map(repository.getHourlyWeather(location.first, location.second), input -> {
            if(input instanceof ApiResponse.ApiSuccessResponse){
                return (WeatherModel)(((ApiResponse.ApiSuccessResponse<WeatherModel>) input).getBody());
            } else if(input instanceof ApiResponse.ApiErrorResponse){
                infoMessage.setValue(((ApiResponse.ApiErrorResponse<WeatherModel>) input).getErrorMessage());
            }
            return new WeatherModel();
        });
    }

    public static class Factory  {
        @Inject
        public Factory(Repository repository) {
            this.repository = repository;
        }
        Repository repository;
        public AbstractSavedStateVMFactory create(Fragment owner)  {
            return new AbstractSavedStateVMFactory(owner, owner.getArguments()) {
                @NonNull
                @Override
                protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
                    return (T) new HourlyViewModel(repository, handle);
                }
            };
        }

    }


}
